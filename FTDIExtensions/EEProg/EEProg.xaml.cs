﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using Utils.Collections;

namespace FTDI.EEProg
{
    /// <summary></summary>
    internal enum Interfaces
    {
        /// <summary></summary>
        I2C,
    }

    /// <summary></summary>
    internal partial class EEProg : Window
    {
        /// <summary></summary>
        private static readonly DependencyPropertyKey ReadOnlyPropPropertyKey = DependencyProperty.RegisterReadOnly("SelectedFilePropery", typeof(string), typeof(EEProg), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.None));

        /// <summary></summary>
        public static readonly DependencyProperty ReadOnlyPropProperty = ReadOnlyPropPropertyKey.DependencyProperty;

        /// <summary></summary>
        public int ReadOnlyProp
        {
            get { return (int)GetValue(ReadOnlyPropProperty); }
            protected set { SetValue(ReadOnlyPropPropertyKey, value); }
        }



        /// <summary></summary>
        public EEProg()
        {
            InitializeComponent();

            cboxSelectInterface.ItemsSource = Enum.GetValues(typeof(Interfaces)).Cast<Interfaces>();
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbarMain_Loaded(object sender, RoutedEventArgs e)
        {
            // Disable overflow and hide overflow button.

            var toolBar = sender as ToolBar;

            var overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }

            var mainPanelBorder = toolBar.Template.FindName("MainPanelBorder", toolBar) as FrameworkElement;
            if (mainPanelBorder != null)
            {
                mainPanelBorder.Margin = new Thickness();
            }
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateDevices_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboxSelectDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboxSelectInterface_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSelectFile_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
