﻿using System;
using System.Runtime.InteropServices;

using FT_STATUS                 = FTDI.NativeWrapper.FTDINative.FT_STATUS;
using FT_DEVICE_LIST_INFO_NODE  = FTDI.NativeWrapper.FTDINative.FT_DEVICE_LIST_INFO_NODE;


namespace FTDI.NativeWrapper
{
    /// <summary></summary>
    public static class I2CNative
    {
        /// <summary>Used for specifying I2C clock.</summary>
        public enum I2C_CLOCK : uint
        {
            I2C_CLOCK_STANDARD_MODE                     = 100000,
            I2C_CLOCK_FAST_MODE                         = 400000,
            I2C_CLOCK_FAST_MODE_PLUS                    = 1000000,
            I2C_CLOCK_HIGH_SPEED_MODE                   = 3400000,
        };

        /// <summary>Used for specifying I2C options.</summary>
        [Flags]
        public enum I2C_OPTIONS : uint
        {
            I2C_OPTIONS_NONE                            = 0x00000000,

            I2C_DISABLE_3PHASE_CLOCKING                 = 0x00000001,
            I2C_ENABLE_DRIVE_ONLY_ZERO                  = 0x00000002,
        };

        /// <summary>Used for specifying I2C transfer options.</summary>
        [Flags]
        public enum I2C_TRANSFER_OPTIONS : uint
        {
            I2C_TRANSFER_OPTIONS_NONE                   = 0x00000000,

            I2C_TRANSFER_OPTIONS_START_BIT              = 0x00000001,
            I2C_TRANSFER_OPTIONS_STOP_BIT               = 0x00000002,
            I2C_TRANSFER_OPTIONS_BREAK_ON_NACK          = 0x00000004,
            I2C_TRANSFER_OPTIONS_NACK_LAST_BYTE         = 0x00000008,
            I2C_TRANSFER_OPTIONS_FAST_TRANSFER_BYTES    = 0x00000010,
            I2C_TRANSFER_OPTIONS_FAST_TRANSFER_BITS     = 0x00000020,
            I2C_TRANSFER_OPTIONS_NO_ADDRESS             = 0x00000040,
        };


        /// <summary>Channel configuration.</summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct I2C_CHANNEL_CONFIG
        {
            public I2C_CLOCK ClockRate; // 0 .. 3400000
            public byte LatencyTimer;   // 0 .. 255
            public I2C_OPTIONS Options;
        }


        /// <summary>Gets the numbers of I2C channels that are connected to the host system.</summary>
        /// <param name="numChannels"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS I2C_GetNumChannels(out uint numChannels);

        /// <summary>Provides information about the channel for specified index.</summary>
        /// <param name="index"></param>
        /// <param name="chanInfo"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS I2C_GetChannelInfo(uint index, out FT_DEVICE_LIST_INFO_NODE chanInfo);

        /// <summary>Opens the indexed channel and provides handle to it.</summary>
        /// <param name="index"></param>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS I2C_OpenChannel(uint index, out IntPtr handle);

        /// <summary>Initializes specified channel.</summary>
        /// <param name="handle"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS I2C_InitChannel(IntPtr handle, ref I2C_CHANNEL_CONFIG config);

        /// <summary>Closes specified channel.</summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS I2C_CloseChannel(IntPtr handle);

        /// <summary>Reads the specified number of bytes from an addressed I2C slave.</summary>
        /// <param name="handle"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="sizeToTransfer"></param>
        /// <param name="buffer"></param>
        /// <param name="sizeTransfered"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern uint I2C_DeviceRead(IntPtr handle, uint deviceAddress, uint sizeToTransfer, [MarshalAs(UnmanagedType.LPArray)] byte[] buffer, ref uint sizeTransfered, I2C_TRANSFER_OPTIONS options);

        /// <summary>Writes the specified number of bytes to an addressed I2C slave.</summary>
        /// <param name="handle"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="sizeToTransfer"></param>
        /// <param name="buffer"></param>
        /// <param name="sizeTransfered"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern uint I2C_DeviceWrite(IntPtr handle, uint deviceAddress, uint sizeToTransfer, [MarshalAs(UnmanagedType.LPArray)] byte[] buffer, ref uint sizeTransfered, I2C_TRANSFER_OPTIONS options);
    }
}
