﻿using System;
using System.Runtime.InteropServices;


namespace FTDI.NativeWrapper
{
    /// <summary></summary>
    public static class FTDINative
    {
        /// <summary>Used as result of native functions.</summary>
        public enum FT_STATUS : uint
        {
            FT_OK,
            FT_INVALID_HANDLE,
            FT_DEVICE_NOT_FOUND,
            FT_DEVICE_NOT_OPENED,
            FT_IO_ERROR,
            FT_INSUFFICIENT_RESOURCES,
            FT_INVALID_PARAMETER,
            FT_INVALID_BAUD_RATE,

            FT_DEVICE_NOT_OPENED_FOR_ERASE,
            FT_DEVICE_NOT_OPENED_FOR_WRITE,
            FT_FAILED_TO_WRITE_DEVICE,
            FT_EEPROM_READ_FAILED,
            FT_EEPROM_WRITE_FAILED,
            FT_EEPROM_ERASE_FAILED,
            FT_EEPROM_NOT_PRESENT,
            FT_EEPROM_NOT_PROGRAMMED,
            FT_INVALID_ARGS,
            FT_NOT_SUPPORTED,
            FT_OTHER_ERROR,
            FT_DEVICE_LIST_NOT_READY,
        };


        /// <summary>Info about channel.</summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct FT_DEVICE_LIST_INFO_NODE
        {
            public uint Flags;
            public uint Type;
            public uint ID;
            public uint LocId;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
            public string SerialNumber;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string Description;

            IntPtr Handle;
        }


        /// <summary>Function writes to the 8 GPIO lines associated with the high byte of MPSSE channel.</summary>
        /// <param name="handle"></param>
        /// <param name="dir"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint FT_WriteGPIO(IntPtr handle, byte dir, byte value);

        /// <summary>Function reads from 8 GPIO lines associated with the high byte of MPSSE channel.</summary>
        /// <param name="handle"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint FT_ReadGPIO(IntPtr handle, out byte value);

        /// <summary>Initializes the library.</summary>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Init_libMPSSE();

        /// <summary>Cleans up resources used by the library.</summary>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Cleanup_libMPSSE();
    }
}
