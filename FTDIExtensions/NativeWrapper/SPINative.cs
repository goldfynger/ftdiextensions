﻿using System;
using System.Runtime.InteropServices;

using FT_STATUS = FTDI.NativeWrapper.FTDINative.FT_STATUS;
using FT_DEVICE_LIST_INFO_NODE = FTDI.NativeWrapper.FTDINative.FT_DEVICE_LIST_INFO_NODE;


namespace FTDI.NativeWrapper
{
    /// <summary></summary>
    public static class SPINative
    {
        /// <summary>Used for specifying SPI options.</summary>
        [Flags]
        public enum SPI_OPTIONS : uint
        {
            SPI_OPTIONS_NONE                            = 0x00000000,

            SPI_CONFIG_OPTION_MODE0                     = 0x00000000,
            SPI_CONFIG_OPTION_MODE1                     = 0x00000001,
            SPI_CONFIG_OPTION_MODE2                     = 0x00000002,
            SPI_CONFIG_OPTION_MODE3                     = 0x00000003,
            SPI_CONFIG_OPTION_MODE_MASK                 = 0x00000003,

            SPI_CONFIG_OPTION_CS_DBUS3                  = 0x00000000,
            SPI_CONFIG_OPTION_CS_DBUS4                  = 0x00000004,
            SPI_CONFIG_OPTION_CS_DBUS5                  = 0x00000008,
            SPI_CONFIG_OPTION_CS_DBUS6                  = 0x0000000C,
            SPI_CONFIG_OPTION_CS_DBUS7                  = 0x00000010,
            SPI_CONFIG_OPTION_CS_MASK                   = 0x0000001C,

            SPI_CONFIG_OPTION_CS_ACTIVELOW              = 0x00000020,
        };

        /// <summary>Used for specifying SPI transfer options.</summary>
        [Flags]
        public enum SPI_TRANSFER_OPTIONS : uint
        {
            SPI_TRANSFER_OPTIONS_NONE                   = 0x00000000,

            SPI_TRANSFER_OPTIONS_SIZE_IN_BYTES          = 0x00000000,
            SPI_TRANSFER_OPTIONS_SIZE_IN_BITS           = 0x00000001,
            SPI_TRANSFER_OPTIONS_SIZE_MASK              = 0x00000001,

            SPI_TRANSFER_OPTIONS_CHIPSELECT_ENABLE      = 0x00000002,
            SPI_TRANSFER_OPTIONS_CHIPSELECT_DISABLE     = 0x00000004,
            SPI_TRANSFER_OPTIONS_CHIPSELECT_MASK        = 0x00000006,
        };


        /// <summary>Channel configuration.</summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct SPI_CHANNEL_CONFIG
        {
            public uint ClockRate;      // 0 .. 30000000
            public byte LatencyTimer;   // 0 .. 255
            public SPI_OPTIONS Options;
            public uint Pins;
            public ushort Reserved;
        }


        /// <summary>Gets the numbers of SPI channels that are connected to the host system.</summary>
        /// <param name="numChannels"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_GetNumChannels(out uint numChannels);

        /// <summary>Provides information about the channel for specified index.</summary>
        /// <param name="index"></param>
        /// <param name="chanInfo"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_GetChannelInfo(uint index, out FT_DEVICE_LIST_INFO_NODE chanInfo);

        /// <summary>Opens the indexed channel and provides handle to it.</summary>
        /// <param name="index"></param>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_OpenChannel(uint index, out IntPtr handle);

        /// <summary>Initializes specified channel.</summary>
        /// <param name="handle"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_InitChannel(IntPtr handle, ref SPI_CHANNEL_CONFIG config);

        /// <summary>Closes specified channel.</summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_CloseChannel(IntPtr handle);

        /// <summary>Reads the specified number of bits or bytes from a SPI slave.</summary>
        /// <param name="handle"></param>
        /// <param name="buffer"></param>
        /// <param name="sizeToTransfer"></param>
        /// <param name="sizeTransfered"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_Read(IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] byte[] buffer, uint sizeToTransfer, ref uint sizeTransfered, SPI_TRANSFER_OPTIONS options);

        /// <summary>Writes the specified number of bits or bytes to a SPI slave.</summary>
        /// <param name="handle"></param>
        /// <param name="buffer"></param>
        /// <param name="sizeToTransfer"></param>
        /// <param name="sizeTransfered"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_Write(IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] byte[] buffer, uint sizeToTransfer, ref uint sizeTransfered, SPI_TRANSFER_OPTIONS options);

        /// <summary>Reads from and writes to the SPI slave simultaneously. </summary>
        /// <param name="handle"></param>
        /// <param name="inBuffer"></param>
        /// <param name="outBuffer"></param>
        /// <param name="sizeToTransfer"></param>
        /// <param name="sizeTransfered"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_ReadWrite(IntPtr handle, [MarshalAs(UnmanagedType.LPArray)] byte[] inBuffer,
            [MarshalAs(UnmanagedType.LPArray)] byte[] outBuffer, uint sizeToTransfer, ref uint sizeTransfered, SPI_TRANSFER_OPTIONS options);

        /// <summary>Reads the state of the MISO line without clocking the SPI bus.</summary>
        /// <param name="handle"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_IsBusy(IntPtr handle, out bool state);

        /// <summary>Changes the chip select line.</summary>
        /// <param name="handle"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        [DllImport("libMPSSE.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FT_STATUS SPI_ChangeCS(IntPtr handle, SPI_CHANNEL_CONFIG config);
    }
}
