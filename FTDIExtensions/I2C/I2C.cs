﻿using System;

using FTDI.NativeWrapper;

using FT_STATUS = FTDI.NativeWrapper.FTDINative.FT_STATUS;
using FT_DEVICE_LIST_INFO_NODE = FTDI.NativeWrapper.FTDINative.FT_DEVICE_LIST_INFO_NODE;


namespace FTDI.I2C
{
    /// <summary></summary>
    internal sealed class I2C
    {
        /// <summary></summary>
        private enum Commands
        {
            help,
            exit,
            quit,
            count,
            info,
            list,
        }

        /// <summary></summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            try
            {
                FTDINative.Init_libMPSSE();

                MainLoop();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Application exception: {ex.Message}");

                Console.ReadLine();
            }
            finally
            {
                FTDINative.Cleanup_libMPSSE();
            }
        }

        /// <summary></summary>
        private static void MainLoop()
        {
            while (true)
            {
                Console.Write("I2C:>");

                var line = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(line))
                {
                    ShowHelp();

                    continue;
                }

                var lineElements = line.Split(' ');

                if (lineElements.Length == 0)
                {
                    ShowHelp();

                    continue;
                }


                var command = default(Commands);

                try
                {
                    command = (Commands)Enum.Parse(typeof(Commands), lineElements[0].ToLower());
                }
                catch
                {
                    ShowHelp();

                    continue;
                }


                switch (command)
                {
                    case Commands.help:
                        ShowHelp();
                        break;

                    case Commands.exit:
                    case Commands.quit:
                        return;

                    case Commands.count:
                        ShowCount();
                        break;

                    case Commands.info:
                        if (lineElements.Length > 1) ShowInfo(lineElements[1]);
                        else ShowHelp();
                        break;

                    case Commands.list:
                        ShowList();
                        break;

                    default:
                        ShowHelp();
                        break;
                }
            }
        }

        /// <summary></summary>
        private static void ShowHelp()
        {
            Console.WriteLine(
                "Usage:\r\n" +
                $"    {Commands.help}{Environment.NewLine}" +
                $"    {Commands.exit}{Environment.NewLine}" +
                $"    {Commands.quit}{Environment.NewLine}" +
                $"    {Commands.count}{Environment.NewLine}" +
                $"    {Commands.list}{Environment.NewLine}" +
                $"    {Commands.info} %index%"
                );
        }

        /// <summary></summary>
        private static void ShowCount()
        {
            try
            {
                uint numChannels = 0;

                var status = I2CNative.I2C_GetNumChannels(out numChannels);

                if (status != FT_STATUS.FT_OK) throw new ApplicationException($"Native function returned {status.ToString()}");

                Console.WriteLine($"Count of I2C channels: {numChannels}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary></summary>
        /// <param name="indexStr"></param>
        private static void ShowInfo(string indexStr)
        {
            try
            {
                uint index = 0;

                if (!uint.TryParse(indexStr, out index))
                {
                    throw new ApplicationException("Invalid index");
                }


                uint numChannels = 0;

                var status = I2CNative.I2C_GetNumChannels(out numChannels);

                if (status != FT_STATUS.FT_OK) throw new ApplicationException($"Native function returned {status.ToString()}");

                if (index >= numChannels) throw new ApplicationException("No channels with specified index");


                FT_DEVICE_LIST_INFO_NODE chanInfo = default(FT_DEVICE_LIST_INFO_NODE);

                status = I2CNative.I2C_GetChannelInfo(index, out chanInfo);

                if (status != FT_STATUS.FT_OK) throw new ApplicationException($"Native function returned {status.ToString()}");

                Console.WriteLine($"I2C channel {index} info: {chanInfo.Description}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary></summary>
        private static void ShowList()
        {
            try
            {
                uint numChannels = 0;

                var status = I2CNative.I2C_GetNumChannels(out numChannels);

                if (status != FT_STATUS.FT_OK) throw new ApplicationException($"Native function returned {status.ToString()}");

                if (numChannels == 0) throw new ApplicationException("No available channels");


                for (uint index = 0; index < numChannels; index++)
                {
                    FT_DEVICE_LIST_INFO_NODE chanInfo = default(FT_DEVICE_LIST_INFO_NODE);

                    status = I2CNative.I2C_GetChannelInfo(index, out chanInfo);

                    if (status != FT_STATUS.FT_OK) throw new ApplicationException($"Native function returned {status.ToString()}");

                    Console.WriteLine($"I2C channel {index} info: {chanInfo.Description}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
